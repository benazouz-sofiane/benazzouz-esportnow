// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
firebaseConfig :{
  apiKey: "AIzaSyBHXmJejoD44p_Km6P5e7xTITOed8m0AB0",
  authDomain: "e-sport-now.firebaseapp.com",
  projectId: "e-sport-now",
  storageBucket: "e-sport-now.appspot.com",
  messagingSenderId: "164905154914",
  appId: "1:164905154914:web:57eb8fb684b4fd8398943b",
  measurementId: "G-KE767RSKND"
}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
