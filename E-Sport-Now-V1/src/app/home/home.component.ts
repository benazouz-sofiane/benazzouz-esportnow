import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { uid } from 'uid';
import { EComment,/*EPostLike,*/ User, UserPost } from '../model/user';
import { UserService } from '../service/user.service';


@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    users: User[] = [];
    userPosts: UserPost[] = [];

    // texte du post du user
    public newUserPostText: string = '';
    public postCommentText: string;
    public textareaFake: string = '';

    constructor(
    //    private authenticationService: AuthenticationService,
       private userService: UserService
    ) {
        // this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        //     this.currentUser = user;
        // });
    }

    ngOnInit() {
        this.loadAllUsers();
        this.loadAllUserPost();
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

    deleteUser(id: number) {
        // this.userService.delete(id).pipe(first()).subscribe(() => {
        //     this.loadAllUsers()
        // });
    }

    private loadAllUsers() {
        // this.userService.getAll().pipe(first()).subscribe(users => {
            // this.users = users;
        // });
    }

    private loadAllUserPost() {
        this.userPosts = this.userService.dataHome;
        //this.userPosts = this.userService.getAll();
        // this.userService.getAll().pipe(first()).subscribe(users => {
            // this.users = users;
        // });
    }

    createUid(): string {
        return uid(10);
    }

    public myText(event: any){
        this.newUserPostText = event;
    }

    public post(){
        const userPost= new UserPost();
        userPost.description = this.newUserPostText;
        userPost.date = new Date().toLocaleDateString();
        userPost.id = this.createUid();
        userPost.userId = this.userService.currentUser.uid;
        //userPost.nbLike = [];
        //userPost.comments = [];
        this.userService.ajoutePost(userPost)
        .then( res => {
            this.userService.getDataPost().then( _ => {
                this.userPosts = this.userService.dataHome;
            })
            console.log("post ajouté avec succes", res);
        });
    }

   
    public addPostComment(postId: string): void {
        const commentedPost = this.userPosts.find(u => u.id === postId);
        const newComment = new EComment();
        newComment.id = this.createUid();
        newComment.date = new Date().toLocaleDateString();
        newComment.postId = postId;
        newComment.description = this.postCommentText;
        
        newComment.userId = this.userService.currentUser.uid;
        if (commentedPost) {
            if (!commentedPost.comments) {
                commentedPost.comments = [];
            }
            commentedPost.comments.push( Object.assign({}, newComment));
        }
        this.userService.ajoutePostComment(commentedPost as UserPost)
        .then( res => {
            console.log("post ajouté avec succes", res);
        });
    }

    public writeComment( event: any): void {
        this.postCommentText = event.target.value;
        this.textareaFake = '';
    }

    public logout(): void {
        this.userService.logout();
    }
    
}