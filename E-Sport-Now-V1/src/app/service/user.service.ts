import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, UserPost } from '../model/user';
import * as FakeData from '../model/fakeData';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore , AngularFirestoreDocument} from '@angular/fire/firestore';
import { ActivatedRouteSnapshot, Router } from '@angular/router';



@Injectable({ providedIn: 'root' })
export class UserService {
    
    public dataHome : UserPost[];
    public userLogged : boolean = false;
    userData: any;
    public currentUser: any;
    
    
    constructor( 
         public userAuth: AngularFireAuth,
         public postService: AngularFirestore,
         public router : Router
    ){

        // this.userAuth.authState.subscribe(user => {
        //     if (user) {
        //         console.log('user auth', user);
        //       this.userData = user;
        //       localStorage.setItem('user', JSON.stringify(this.userData));
        //       JSON.parse(localStorage.getItem('user') || '{}');
        //     } else {
        //       localStorage.setItem('user', '');
        //       JSON.parse(localStorage.getItem('user') || '{}');
        //     }
        //   })
    }

    SetUserData(user: firebase.default.auth.UserCredential) {
        console.log(user.user?.uid);
        const userRef: AngularFirestoreDocument<any> = this.postService.doc('users/${user.user?.uid}');
        console.log(userRef);
        const userData: User = {
          id: user.user?.uid as string,
          username: user.user?.email as string
        }
        return userRef.set(userData, {
          merge: true
        })
      }


    logout() {
       // this.userAuth.signOut().then(_ => {
            this.userLogged = false;

            localStorage.removeItem('user')
            this.router.navigate(['login']);
        //});
   }
    SignIn(email:string,password:string):Promise<firebase.default.auth.UserCredential>{
        return new Promise<any>((resolve,reject) => { 
            this.userAuth.signInWithEmailAndPassword(email,password).then(res=>{
                this.userLogged = true;
                // this.SetUserData(res);
                this.currentUser = res.user;
                //console.log( 'user data local:', JSON.parse(localStorage.getItem('user') || '{}'))
                resolve(res.user)
                
            });
        });
    }

    getAll() {
        // TODO appeler la base depuis firebase

        return ;
    }

    register(email:string,password:string) {
        return this.userAuth.createUserWithEmailAndPassword(email,password);
    }

    delete(id: number) {
        
    }

    getDataPost(){
        
         return new Promise<any>((resolve,reject) => {
             this.postService.collection("posts").valueChanges().subscribe(data => {
                 this.dataHome = data as UserPost[]; 
                 resolve(this.dataHome);
             });
    });
}

    ajoutePost(post: UserPost) {
        return new Promise<any>((resolve, reject) => {
            this.postService
                .collection("posts")
                .doc(post.id)
                .set(Object.assign({}, post))
                .then(res => {
                    console.log('lu', res);
                    resolve(res);
                });
        });
      }

      ajoutePostComment(post: UserPost) {

        return new Promise<any>((resolve, reject) => {
            this.postService
                .collection("posts")
                .doc(post.id)
                .update({comments: post.comments})
                .then(res => {
                    console.log('lu', res);
                    resolve(res);
                });
        });
      }


      canActivate(route : ActivatedRouteSnapshot): boolean {
        if(this.userLogged){
            return true;
        }
        this.router.navigate(['login']);
        return false;
      }
}