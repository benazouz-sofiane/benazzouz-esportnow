export class User {
    id: string;
    username: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    token?: string;
}

export class UserPost {
    id: string;
    userId: string;
    date: string;
    description: string;
    title: string;
    comments?: EComment[];
    //nbLike: EPostLike[];
}

export class EComment{
    id: string;
    userId: string;
    postId: string;
    description: string;
    date: string;
}


